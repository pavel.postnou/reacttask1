import React, {Component} from 'react';

class Mass extends Component {
    constructor(props) {
        super(props);
        this.state = {
            arr:[".",".","."]
        }

    }

    render() {
        return (
            <div>
                <div>
                {this.state.arr}
                </div>
                <button onClick={() => this.handleClick(1)}>1</button>
                <button onClick={() => this.handleClick(2)}>2</button>
                <button onClick={() => this.handleClick(3)}>3</button>
                <button onClick={() => this.handleClick(4)}>4</button>
                <button onClick={() => this.handleClick(5)}>5</button>
            </div>
        )
    }

    handleClick = (i) => {
        this.state.arr.push(i)
        this.state.arr.shift()
        this.setState({
        arr:this.state.arr
        })
    }

}

export default Mass;
